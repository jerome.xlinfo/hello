FROM busybox
RUN mkdir -p /usr/local/sbin
COPY hello /usr/local/sbin
ENV arg="le monde"
ENTRYPOINT /usr/local/sbin/hello $arg

